﻿using System;
namespace week2
{
    class Program
    {
        static void Main()
        {
            string input = "   ĐÂY LÀ MỘT CHUỖI VÍ DỤ   ";
            // Format the string using the FormatString method
            string output = MyStringFormatter.FormatString(input);
            
            // Print the formatted string
            Console.WriteLine(output);
        }
    }
}
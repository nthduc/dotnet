﻿namespace week2
{
    public  class MyStringFormatter
    {
        /// <summary>
        /// Định dạng chuỗi theo yêu cầu.
        /// </summary>
        /// <param name="input">Chuỗi đầu vào.</param>
        /// <returns>Chuỗi đã được định dạng.</returns>
        public static string FormatString(string input)
        {
            // Cắt bỏ hết các khoảng trắng dư ở đầu cuối chuỗi
            input = input.Trim();

            // Thay thế các khoảng trắng liên tiếp bằng một khoảng trắng
            while (input.IndexOf("  ") >= 0)
                input = input.Replace("  ", " ");

            // Tách chuỗi thành các từ
            string[] words = input.Split(' ');

            // Viết hoa chữ cái đầu tiên của mỗi từ và viết thường các chữ cái còn lại
            for (int i = 0; i < words.Length; i++)
                words[i] = words[i].Substring(0, 1).ToUpper() + words[i].Substring(1).ToLower();

            // Nối các từ lại thành một chuỗi
            return string.Join(" ", words);
        }
    }
}
﻿using System;
using System.Collections.Generic;

namespace week1
{

    class Student
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public Student(string name, int age)
        {
            Name = name;
            Age = age;
        }
    }

    class QLSV
    {
        public static void QuanLiSinhVien()
        {
            var students = new List<Student>();

            while (true)
            {
                Console.WriteLine("----- Quản lý sinh viên -----");
                Console.WriteLine("1. Thêm sinh viên");
                Console.WriteLine("2. Hiển thị danh sách sinh viên");
                Console.WriteLine("3. Thoát");

                int choice = Convert.ToInt32(Console.ReadLine());

                switch (choice)
                {
                    case 1:
                        Console.WriteLine("Nhập tên sinh viên: ");
                        var name = Console.ReadLine();

                        Console.WriteLine("Nhập tuổi sinh viên: ");
                        var age = Convert.ToInt32(Console.ReadLine());

                        var student = new Student(name, age);
                        students.Add(student);

                        Console.WriteLine("Thêm sinh viên thành công!");
                        break;
                    case 2:
                        Console.WriteLine("Danh sách sinh viên:");
                        foreach (var std in students)
                        {
                            Console.WriteLine("Tên: " + std.Name);
                            Console.WriteLine("Tuổi: " + std.Age);
                            Console.WriteLine("-----------------------");
                        }

                        break;
                    case 3:
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("Lựa chọn không hợp lệ!");
                        break;
                }
            }
        }
    }
}

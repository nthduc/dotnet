﻿using System;

namespace week1
{
    class PhuongTrinhBac2
    {
        public static void PtBac2()
        {
            Console.WriteLine("Nhập hệ số a:");
            var a = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Nhập hệ số b:");
            var b = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Nhập hệ số c: ");
            var c = Convert.ToDouble(Console.ReadLine());

            double delta = b * b - 4 * a * c;

            if (delta < 0)
            {
                Console.WriteLine("Phương trình vô nghiệm");
            }
            else if (delta == 0)
            {
                double x = -delta / 2 * a;
                Console.WriteLine("Phương trình có 2 nghiệm kép là:" + x);
            }
            else
            {
                double x1 = (-b + Math.Sqrt(delta)) / (2 * a);
                double x2 = (-b - Math.Sqrt(delta)) / (2 * a);
                Console.WriteLine("Phương trình có hai nghiệm phân biệt:");
                Console.WriteLine("x1 = " + x1);
                Console.WriteLine("x2 = " + x2);
            }
        }
    }
}
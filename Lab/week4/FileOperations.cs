﻿using System.Text.RegularExpressions;
using System;
using System.IO;

namespace week4
{
 

    public class FileOperations
    {
        public static bool DeleteAllFiles(string path)
        {
            try
            {
                DirectoryInfo directory = new DirectoryInfo(path);
                foreach (FileInfo file in directory.GetFiles())
                {
                    file.Delete();
                }
                foreach (DirectoryInfo subDirectory in directory.GetDirectories())
                {
                    subDirectory.Delete(true);
                }
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public static void FindAll(string path, string extension)
        {
            try
            {
                DirectoryInfo directory = new DirectoryInfo(path);
                FileInfo[] files = directory.GetFiles("*." + extension, SearchOption.AllDirectories);
                foreach (FileInfo file in files)
                {
                    Console.WriteLine(file.FullName);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public static void FindAllRegex(string path, string pattern)
        {
            try
            {
                DirectoryInfo directory = new DirectoryInfo(path);
                FileInfo[] files = directory.GetFiles("*.*", SearchOption.AllDirectories);
                Regex regex = new Regex(pattern);
                foreach (FileInfo file in files)
                {
                    if (regex.IsMatch(file.Name))
                    {
                        Console.WriteLine(file.FullName);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
﻿using System;

namespace week4
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = "F:\\Document\\.NET";
            string pattern = @"\.(txt|docx)$";

            Console.WriteLine("Deleting all files and subdirectories in {0}...", path);
            if (FileOperations.DeleteAllFiles(path))
            {
                Console.WriteLine("Done.");
            }
            else
            {
                Console.WriteLine("An error occurred.");
            }

            Console.WriteLine("\nFinding all files in {0} matching pattern {1}...", path, pattern);
            FileOperations.FindAll(path, pattern);

            Console.ReadLine();
        }
    }
}
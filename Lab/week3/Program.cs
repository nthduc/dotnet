﻿using System;

namespace week3
{
    class Program
    {
        static void Main(string[] args)
        {
            QuanLySinhVien quanLy = new QuanLySinhVien();

            SinhVien sv1 = new SinhVien("SV001", "Nguyen Van A", "Nam", new DateTime(2000, 1, 1));
            sv1.ThemMonHoc(new MonHoc("MH001", 3, "Toan", "GV001", 9.0));
            sv1.ThemMonHoc(new MonHoc("MH002", 2, "Ly", "GV002", 8.5));
            quanLy.ThemSinhVien(sv1);

            SinhVien sv2 = new SinhVien("SV002", "Tran Thi B", "Nu", new DateTime(2000, 2, 2));
            sv2.ThemMonHoc(new MonHoc("MH001", 3, "Toan", "GV001", 7.5));
            sv2.ThemMonHoc(new MonHoc("MH003", 2, "Hoa", "GV003", 8.0));
            quanLy.ThemSinhVien(sv2);

            Console.WriteLine("Danh sach sinh vien truoc khi sap xep:");
            foreach (SinhVien sv in quanLy.DSSinhVien)
            {
                Console.WriteLine("{0} - {1} - DTB: {2}", sv.MSSV, sv.HoVaTen, sv.DiemTrungBinh());
            }

            quanLy.SapXepTheoDiemTrungBinh();

            Console.WriteLine("\nDanh sach sinh vien sau khi sap xep:");
            foreach (SinhVien sv in quanLy.DSSinhVien)
            {
                Console.WriteLine("{0} - {1} - DTB: {2}", sv.MSSV, sv.HoVaTen, sv.DiemTrungBinh());
            }

            Console.ReadLine();
        }

    }
}
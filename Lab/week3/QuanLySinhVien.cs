﻿using System.Collections.Generic;

namespace week3
{
    class QuanLySinhVien
    {
       public List<SinhVien> DSSinhVien = new List<SinhVien>();

        public void ThemSinhVien(SinhVien sinhVien)
        {
            this.DSSinhVien.Add(sinhVien);
        }

        public void XoaSinhVien(SinhVien sinhVien)
        {
            this.DSSinhVien.Remove(sinhVien);
        }

        public SinhVien TimKiemTheoMSSV(string mssv)
        {
            foreach (SinhVien sinhVien in DSSinhVien)
            {
                if (sinhVien.MSSV == mssv)
                {
                    return sinhVien;
                }
            }
            return null;
        }

        public void SapXepTheoDiemTrungBinh()
        {
            DSSinhVien.Sort((x, y) => x.DiemTrungBinh().CompareTo(y.DiemTrungBinh()));
        }
    }
}
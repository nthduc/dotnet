﻿namespace week3
{
    class MonHoc
    {
        public string MSMH { get; set; }
        public int SoTinChi { get; set; }
        public string TenMonHoc { get; set; }
        public string MSGV { get; set; }
        public double Diem { get; set; }

        public MonHoc(string msmh, int soTinChi, string tenMonHoc, string msgv, double diem)
        {
            this.MSMH = msmh;
            this.SoTinChi = soTinChi;
            this.TenMonHoc = tenMonHoc;
            this.MSGV = msgv;
            this.Diem = diem;
        }
    }

}
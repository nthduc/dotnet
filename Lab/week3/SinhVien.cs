﻿using System;
using System.Collections.Generic;

namespace week3
{
    class SinhVien
    {
        public string MSSV { get; set; }
        public string HoVaTen { get; set; }
        public string GioiTinh { get; set; }
        public DateTime NgaySinh { get; set; }
        public List<MonHoc> DSMonHoc { get; set; }

        public SinhVien(string mssv, string hoVaTen, string gioiTinh, DateTime ngaySinh)
        {
            this.MSSV = mssv;
            this.HoVaTen = hoVaTen;
            this.GioiTinh = gioiTinh;
            this.NgaySinh = ngaySinh;
            this.DSMonHoc = new List<MonHoc>();
        }

        public void ThemMonHoc(MonHoc monHoc)
        {
            this.DSMonHoc.Add(monHoc);
        }

        public void XoaMonHoc(MonHoc monHoc)
        {
            this.DSMonHoc.Remove(monHoc);
        }

        public double DiemTrungBinh()
        {
            double tongDiem = 0;
            int tongSoTinChi = 0;
            foreach (MonHoc monHoc in DSMonHoc)
            {
                tongDiem += monHoc.SoTinChi * monHoc.Diem;
                tongSoTinChi += monHoc.SoTinChi;
            }
            return tongDiem / tongSoTinChi;
        }
    }
}
﻿using System;
using System.Collections.Generic;

namespace qlnk
{
    class Program
    {
        static void Main(string[] args)
        {
            // Tạo một đối tượng QuanLyNhanVien để quản lý các hoạt động trong kho
            QuanLyNhanVien quanLyNhanVien = new QuanLyNhanVien();

            // Thêm một số tài khoản Admin và Staff để đăng nhập
            quanLyNhanVien.ThemNguoiDung(new Admin("AD001", "Nguyen Van A", "admin", "matkhau"));
            quanLyNhanVien.ThemNguoiDung(new Staff("NV001", "Tran Thi B", "staff", "matkhau"));

            // Đăng nhập với tài khoản và mật khẩu
            Console.Write("Nhập tài khoản: ");
            string taiKhoan = Console.ReadLine();
            Console.Write("Nhập mật khẩu: ");
            string matKhau = Console.ReadLine();
            Person nguoiDung = quanLyNhanVien.DangNhap(taiKhoan, matKhau);
            if (nguoiDung == null)
            {
                Console.WriteLine("Đăng nhập thất bại.");
                return;
            }
            Console.WriteLine("Đăng nhập thành công với vai trò {0}.", nguoiDung is Admin ? "Admin" : "Staff");

            // Hiển thị menu lựa chọn
            bool isRunning = true;
            while (isRunning)
            {
                Console.WriteLine("\nMenu:");
                Console.WriteLine("1. Thêm nhân viên");
                Console.WriteLine("2. Xem thông tin nhân viên");
                Console.WriteLine("3. Xem danh sách nhân viên");
                Console.WriteLine("4. Tìm kiếm nhân viên theo tên");
                Console.WriteLine("5. Xóa nhân viên");
                Console.WriteLine("6. Thoát");
                Console.Write("\nNhập lựa chọn của bạn: ");
                int choice = Convert.ToInt32(Console.ReadLine());

                switch (choice)
                {
                    case 1:
                        // Thêm nhân viên
                        Console.Write("\nNhập ID: ");
                        string id = Console.ReadLine();
                        Console.Write("Nhập họ tên: ");
                        string hoTen = Console.ReadLine();
                        Console.Write("Nhập tài khoản: ");
                        string taiKhoanThem = Console.ReadLine();
                        Console.Write("Nhập mật khẩu: ");
                        string matKhauThem = Console.ReadLine();
                        quanLyNhanVien.ThemNguoiDung(new Staff(id, hoTen, taiKhoanThem, matKhauThem));
                        Console.WriteLine("Đã thêm nhân viên.");
                        break;
                    case 2:
                        // Xem thông tin người dùng
                        Console.Write("\nNhập ID người dùng cần xem: ");
                        string idXem = Console.ReadLine();
                        Person person = quanLyNhanVien.XemNguoiDung(idXem);
                        if (person != null)
                        {
                            Console.WriteLine("Thông tin người dùng: {0} - {1}", person.Id, person.FullName);
                        }
                        else
                        {
                            Console.WriteLine("Không tìm thấy người dùng.");
                        }
                        break;
                    case 3:
                        // Xem tất cả người dùng
                        Console.WriteLine("\nDanh sách tất cả người dùng:");
                        quanLyNhanVien.XemTatCaNguoiDung();
                        break;
                    case 4:
                        // Tìm kiếm người dùng theo tên
                        Console.Write("\nNhập tên người dùng cần tìm: ");
                        string ten = Console.ReadLine();
                        Person personTimKiem = quanLyNhanVien.TimNguoiDungTheoTen(ten);
                        if (personTimKiem != null)
                        {
                            Console.WriteLine("Tìm thấy người dùng: {0} - {1}", personTimKiem.Id, personTimKiem.FullName);
                        }
                        else
                        {
                            Console.WriteLine("Không tìm thấy người dùng.");
                        }
                        break;
                    case 5:
                        // Xóa người dùng
                        Console.Write("\nNhập ID người dùng cần xóa: ");
                        string idXoa = Console.ReadLine();
                        quanLyNhanVien.XoaNguoiDung(idXoa);
                        Console.WriteLine("Đã xóa người dùng.");
                        break;
                    case 6:
                        // Thoát
                        isRunning = false;
                        break;
                    default:
                        Console.WriteLine("Lựa chọn không hợp lệ.");
                        break;
                }
            }

            Console.ReadLine();
        }
    }
}

﻿using System.Collections.Generic;
using System;

namespace qlnk
{
    public class QuanLyNhanVien
    {
        private List<Person> dsNguoiDung = new List<Person>();
        
        public void ThemNguoiDung(Person nguoiDung)
        {
            this.dsNguoiDung.Add(nguoiDung);
        }

        public Person DangNhap(string taiKhoan, string matKhau)
        {
            foreach (Person nguoiDung in dsNguoiDung)
            {
                if (nguoiDung.Account == taiKhoan && nguoiDung.ComparePass(matKhau))
                {
                    return nguoiDung;
                }
            }
            return null;
        }

        public void XoaNguoiDung(string id)
        {
            this.dsNguoiDung.RemoveAll(nd => nd.Id == id);
        }

        public Person XemNguoiDung(string id)
        {
            return this.dsNguoiDung.Find(nd => nd.Id == id);
        }

        public void XemTatCaNguoiDung()
        {
            foreach (Person nguoiDung in dsNguoiDung)
            {
                Console.WriteLine("{0} - {1}", nguoiDung.Id, nguoiDung.FullName);
            }
        }

        public Person TimNguoiDungTheoTen(string ten)
        {
            return this.dsNguoiDung.Find(nd => nd.FullName == ten);
        }
        
    }
    
}
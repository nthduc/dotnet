﻿using System;
using System.Collections.Generic;

namespace qlnk
{
    public class Staff : Person
    {
        public static List<Staff> StaffList = new List<Staff>();

        public Staff(string id, string fullName, string account, string password)
            : base(id, fullName, account, password)
        {
        }

        public static void AddStaff(Staff staff)
        {
            StaffList.Add(staff);
        }

        public static void DeleteStaff(Staff staff)
        {
            StaffList.Remove(staff);
        }

        public static void ViewAllStaff()
        {
            foreach (Staff staff in StaffList)
            {
                Console.WriteLine("{0} - {1}", staff.Id, staff.FullName);
            }
        }

        public static Staff FindStaffByName(string name)
        {
            foreach (Staff staff in StaffList)
            {
                if (staff.FullName == name)
                {
                    return staff;
                }
            }
            return null;
        }
    }
}
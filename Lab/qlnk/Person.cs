﻿namespace qlnk
{
    public abstract class Person
    {
        public string Id { get; set; }
        public string FullName { get; set; }
        public string Account { get; set; }
        public string Password { get; set; }

        public Person(string id, string fullName, string account, string password)
        {
            this.Id = id;
            this.FullName = fullName;
            this.Account = account;
            this.Password = password;
        }

        public string GetId()
        {
            return this.Id;
        }

        public string GetFullName()
        {
            return this.FullName;
        }

        public string GetAccount()
        {
            return this.Account;
        }

        public bool ComparePass(string password)
        {
            return this.Password == password;
        }
    }
}